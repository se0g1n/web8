$(document).ready(function () {
    document.querySelector("input").value =
    localStorage.getItem("saveForm1");
    document.querySelector(".text").value =
    localStorage.getItem("saveForm2");
    document.querySelector("textarea").value =
    localStorage.getItem("saveForm3");
    setInterval(() => { 
    localStorage.setItem("saveForm1",
    document.querySelector("input").value);
    localStorage.setItem("saveForm2",
    document.querySelector(".text").value);
    localStorage.setItem("saveForm3",
    document.querySelector("textarea").value);
    }, 500);
    
    $(".mymagicoverbox").click(function () {
        var page = 2;
        history.pushState({ page: 2 }, "title 2", "form");
        MessageForm(page);
        return false;
    });
    function SaveUsersData(){
        alert("tak");
        localStorage.setItem("saveForm1",
        document.querySelector("input").value);
        localStorage.setItem("saveForm2",
        document.querySelector(".text").value);
        localStorage.setItem("saveForm3",
        document.querySelector("textarea").value);
    }
    function MessageForm(page) {
        if (page === 2) {
            $("#myfond_gris").fadeIn(300);
            $("#box_1").fadeIn(300);
            $("#myfond_gris").attr("opendiv", iddiv);
            return false;
        }
        else {
            var iddiv = $("#myfond_gris").attr("opendiv");
            $("#myfond_gris").fadeOut(300);
            $("#box_1").fadeOut(300);
            history.pushState({ page: 1 }, "title 1", "./");
            return false;
        }
    }

    $("#myfond_gris, .mymagicoverbox_fermer").click(function () {
        var page = 1;
        history.pushState({ page: 1 }, "title 1", "./");
        MessageForm(page);
        return false;
    });

    $(function () {
        $(".ajaxForm").submit(function (e) {
            e.preventDefault();
            var href = $(this).attr("action");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: href,
                data: $(this).serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        alert("We received your submission, thank you!");
                    } else {
                        alert("An error occured: " + response.message);
                    }
                }
            });
            localStorage.clear();
            document.querySelector("input").value =
            localStorage.getItem("saveForm1");
            document.querySelector(".text").value =
            localStorage.getItem("saveForm2");
            document.querySelector("textarea").value =
            localStorage.getItem("saveForm3");
        });
    });
    window.addEventListener("popstate", e => {
        MessageForm(e.state.page);
    });
    history.replaceState({page: null}, "Default state", "./");
});
